package cz.muni.ics.kypo.training.api.enums;

public enum LevelState {
    FINISHED, RUNNING
}
