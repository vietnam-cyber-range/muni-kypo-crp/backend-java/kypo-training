package cz.muni.ics.kypo.training.api.enums;

/**
 * The enumeration of Assessment level types.
 *
 */
public enum AssessmentType {

    /**
     * Test assessment type.
     */
    TEST,
    /**
     * Questionnaire assessment type.
     */
    QUESTIONNAIRE;
}
