package cz.muni.ics.kypo.training.persistence.model.enums;

public enum SubmissionType {
    CORRECT,
    INCORRECT;
}
