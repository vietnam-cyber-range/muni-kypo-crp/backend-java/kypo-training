package cz.muni.ics.kypo.training.persistence.model.enums;

public enum QuestionType {
    /**
     * Free form question
     */
    FFQ,
    /**
     * Extended matching items
     */
    EMI,
    /**
     * Multiple choice question
     */
    MCQ;
}
