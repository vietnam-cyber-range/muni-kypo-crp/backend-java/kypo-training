package cz.muni.ics.kypo.training.persistence.model.enums;

/**
 * Enumeration of Level types.
 *
 */
public enum LevelType {

    /**
     * Assessment level type.
     */
    ASSESSMENT,
    /**
     * Info level type.
     */
    INFO,
    /**
     * Training level type.
     */
    TRAINING,
    /**
     * Access level type.
     */
    ACCESS;
}
