package cz.muni.ics.kypo.training.persistence.model.enums;

/**
 * The enumeration of Command Types.
 *
 */
public enum CommandType {

    /**
     * represents a command used in bash console.
     */
    BASH,
    /**
     * represents a command used in msfconsole.
     */
    MSF;
}